# TD BigData : Kafka + Nifi + ElasticSearch + Kibana


## Etape 1 : Création de l'environnement VM

### Préparation de la machine virtuelle

Créer une VM avec les caractèristiques suivantes : 
   - hostname : $group-data01
   - disque : 60 Go
   - ram : 16Go
   - cpu : 4 socket, 4 cores
   - OS : [Ubuntu Server 22.04 LTS](https://releases.ubuntu.com/22.04.3/ubuntu-22.04.3-live-server-amd64.iso)


### Installer les paquets supplémentaires

Configurer vos proxy pour aller récupérer les paquets apt sur les dépots internet.
Editer le fichier /etc/apt/apt.conf.d/proxy.conf :
  ```
  Acquire::http::Proxy "http://<user>:<password>@<proxyserver>:<proxyport>/";
  Acquire::https::Proxy "http://<user>:<password>@<proxyserver>:<proxyport>/";
  ```


Vérifier que vous pouvez bien installer vos paquets : 
```
# apt -y update
# apt -y upgrade
```

Installer les packages docker : 
```
# apt -y install docker.io 
```

Configurer les proxy pour dockerd en créant le fichier /etc/systemd/system/docker.service.d/http-proxy.conf :
```
[Service]
Environment="HTTP_PROXY=http://<user>:<password>@<proxyserver>:<proxyport>/"
Environment="HTTPS_PROXY=http://<user>:<password>@<proxyserver>:<proxyport>/"
```

### Création compte docker hub

Nous allons faire un usage relativement massif de Dockerhub, afin d'éviter d'atteindre les limites de pull, [créer un compte sur DockerHub](https://hub.docker.com/signup).
Une fois le compte créé vous devez vous connecter à l'aide de la commande [docker login](https://docs.docker.com/engine/reference/commandline/login/)

### Validation étape 1

A faire valider : 

- [ ] Machine virtuelle OK
- [ ] Docker login OK
- [ ] Pull de container (ex : nginx:latest) OK

## Etape 2 : Création de l'environnement container

Nous lancons ici un environnement minimum pour tester le fonctionnement des composants, nous mettons donc de côté les aspects sécurisation (TLS, haute disponibilité, authentification) afin d'avoir un ensemble opérationnel le plus rapidement possible.

### Lancer Kafka et vérifier qu'il est opérationnel 

Utiliser l'image docker bitnami/kafka:latest disponible sur [DockerHub](https://hub.docker.com/r/bitnami/kafka)

```
docker run -d --name kafka \
    # options à mettre ici \
    -p 9092:9092 \
    bitnami/kafka:latest
```

### Lancer Nifi et vérifier qu'il est opérationnel 

Utiliser l'image docker apache/nifi:1.28.1 disponible sur [DockerHub](https://hub.docker.com/r/apache/nifi:1.28.1)

```
docker run -d --name nifi \
    # options à mettre ici
    -p 8443:8443 \
    apache/nifi
```

Vous devez pouvoir accéder à l'IHM de Nifi via http://$ip_vm:8443/

### Lancer ElasticSearch et vérifier qu'il est opérationnel 

Utiliser l'image docker bitnami/elasticsearch:latest disponible sur [DockerHub](https://hub.docker.com/r/bitnami/elasticsearch)

```
docker run -d --name elasticsearch  \
    # options à mettre ici
    -p 9200:9200 \
    bitnami/elasticsearch:latest
```

Vous devez pouvoir accéder aux API Elasticsearch via http://$ip_vm:9200/_cluster/health?pretty

### Lancer Kibana et vérifier qu'il est opérationnel

Utiliser l'image docker bitnami/kibana:latest disponible sur [DockerHub](https://hub.docker.com/r/bitnami/kibana).
Kibana se connecte à ElasticSearch sur le port 9200.
Ne mettre que la configuration minimal pour que cela fonctionne.

```
docker run -d --name kibana \
    # options à mettre ici
    -p 5601:5601 \
    bitnami/kibana:latest
```

Vous devez pouvoir accéder à l'IHM Kibana via http://$ip_vm:5601/

### Validation étape 2

A faire valider : 

- [ ] Container Kafka OK
- [ ] Container Nifi + accès IHM OK
- [ ] Container Elastic + accès API OK
- [ ] Container Kibana + accès IHM OK


## Etape 3 : Emission de messages vers Kafka


### Création d'un topic Kafka et emission d'evenements tests

Installer [Kafkacat](https://manpages.ubuntu.com/manpages/focal/man1/kafkacat.1.html) sur votre VM 

```
# apt install kafkacat
```

Emettre ensuite un événement Kafka 'test' sur le topic 'but3'.
Penser à revoir vos options de lancement de Kafka si cela ne fonctionne pas.
```
# kafkacat -P -t but3 -b $ip_vm:9092
```

### Récupération d'un dataset

Récupérer les jeux de données [Prix des carburants](https://www.prix-carburants.gouv.fr/rubrique/opendata/) sur les années 2007 à 2024.


Dézipper chacun des jeux de données et les concatenner dans un seul fichier 'carburant.xml' :
```
# gunzip dataset.gz
# cat file1 >> carburant.xml
# cat file2 >> carburant.xml
# ...
```


### Streaming du dataset sur Kafka

Les fichiers XML contiennent des objets <pdv> qui représentent un point de vente.


Installer le client Kafka pour python3 : 
```
# apt install python3-pykafka
```


S'appuyer sur le code python ci dessous pour réaliser un script qui va transmettre chaque pdv dans le topic kafka 'but3' en tant qu'événement.

```
from xml.etree import ElementTree
from pykafka import KafkaClient

xmlctx = ElementTree.iterparse('filname.xml', events=('end', ))
kclient = KafkaClient(hosts="$ip_vm:9092")

ktopic = kclient.topics['but3']
with ktopic.get_sync_producer() as producer:
    for event, elem in xmlctx:
    if elem.tag == 'pdv':
        producer.produce(ElementTree.tostring(elem))

```

### Validation étape 3

A faire valider : 

- [ ] Test d'insertion/lecture d'un message avec kafkacat OK
- [ ] Insertion du jeux de donnée dans Kafka OK


## Etape 4 : Consommation des événements Kafka


### Connexion Nifi / Kafka

Depuis votre interface Nifi, créer un consommateur Kafka configuré pour se connecter à votre topic 'but3' et pour utiliser.
Utiliser le processor [ConsumeKafka](https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi/nifi-kafka-2-6-nar/1.28.0/org.apache.nifi.processors.kafka.pubsub.ConsumeKafka_2_6/index.html) 

Ce processor lira les données en XML dans le topic (XMLReader servicecontroller) et les écrire en JSON en sortie (JsonRecordSetWritter servicecontroller).

### Définition de l'objet événement cible

Nous allons scynder un objet 'pdv' complexe en plusieurs objets 'pdvevent' plus simples.
Dans la registry de Nifi, créer un schéma 'pdvevent' qui sera une description au format [Avro](https://fr.wikipedia.org/wiki/Apache_Avro) de votre objet pdvevent cible.

Il contiendra les attributs suivants dans chaque objet pdvevent :  
  - idpdv : identifiant numérique du pdv
  - location : location du pdv [GeoPoint](https://datatracker.ietf.org/doc/html/rfc7946#appendix-A.1)
  - datetime : date du changement de prix [Datetime ISO 8601](https://en.wikipedia.org/wiki/ISO_8601#Times)
  - carburant : Enumération (Gazole, SP95, SP98, E10, E85, GPLc)
  - price : nouveau tarif du carburant
  - cp : code postal du pdv
  - departement : numero de département du pdv (ex : 14)


### Nettoyage de l'evenement

A l'aide du processor [RemoveRecordField](https://nifi.apache.org/documentation/nifi-2.0.0-M1/components/org.apache.nifi/nifi-standard-nar/2.0.0-M1/org.apache.nifi.processors.standard.RemoveRecordField/additionalDetails.html), supprimer toutes les données de l'évenement qui ne seront pas utiles pour la composition de votre objet pdvevent.

### Validation étape 4

A faire valider : 

- [ ] Consommation du topic kafka par Nifi OK
- [ ] Schema avro pdvevent OK
- [ ] Nettoyage de l'évenément avec Nifi OK

## Etape 5 : Transformation des evenements en pdvevent

Pour arriver à vos fin, utiliser les [processors Nifi](https://nifi.apache.org/documentation/v2/) qui permettent de transformer les événements au fil de l'eau (stream processing).
  
### Création des pdvevents

Sur la base d'un événement pdv, créer autant de pdvevent que nécessaire, c'est à dire un pour chaque changement de prix d'un carburant.
Pour cela utiliser [ForkRecord](https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi/nifi-standard-nar/1.7.0/org.apache.nifi.processors.standard.ForkRecord/index.html) qui permet de créer plusieurs événements en splittant un tableau json par exemple.

### Transformation des dates 

Utiliser le processor [UpdateRecord](https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi/nifi-standard-nar/1.25.0/org.apache.nifi.processors.standard.UpdateRecord/index.html) afin de modifier le format de la date et l'écrire au format [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601) dans le bon champ.


Exemple : 
```
"datetime" : "2024-01-02T00:37:00Z"
```

### Transformation des données de localisation

Réécrire les données de géolocalisation et les positionner dans le champ location comme dans l'exemple ci dessous.
Pour cela utiliser le processor [JoltTransformJSON](https://nifi.apache.org/components/org.apache.nifi.processors.jolt.JoltTransformJSON/), qui permet  de réaliser des transformations complexes sur les evenements JSON.

Exemple GeoPoint : 
```
"location": { 
    "lat": 41.12,
    "lon": -71.34
  }
```

### Validation des objet pdvevent

Une fois toutes vos transformations réalisées, vérifier que votre objet cible est bien conforme à l'objet pdfevent décrit lors de l'étape 4.
Pour cela intégrer un contrôle avec le processor [ValidateRecord](https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi/nifi-standard-nar/1.25.0/org.apache.nifi.processors.standard.ValidateRecord/index.html).


### Validation étape 5
- [ ] Création de plusieurs évenements sur la base d'un événement OK
- [ ] Réécriture de la date au format iso 8601
- [ ] Réécriture de la latitude/longitude au format geopoint
- [ ] Validation de l'objet pdvevent avec le schéma Avro OK

## Etape 6 : Ecriture des pdvevents dans elasticsearch

Lors de cette étape, les événements pdvevent seront insérés dans le moteur de stockage elasticsearch sous forme de documents.
Les documents seront stockés dans l'index pdvevent.

### Création du mapping Elasticsearch

Afin de définir la structure des documents de l'index pdvevent, il faut lui appliquer le [mapping](https://www.elastic.co/guide/en/elasticsearch/reference/current/explicit-mapping.html) suivant : 

``` 
# curl -XPUT http://$ip_vm:9200/pdvevent -d '
{
  "mappings": {
    "properties": {
      "idpdv": { "type": "integer"},  
      "location": { "type": "geo_point"}, 
      "datetime": { "type": "date"  },
      "carburant": { "type": "keyword" },
      "price": { "type": "float" },
      "cp": {"type": "integer"}
    }
  }
}'
```

### Ecriture des événements dans elasticsearch 

Utiliser le processor : 

 - [PutElasticSearchRecord](https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi/nifi-elasticsearch-restapi-nar/1.25.0/org.apache.nifi.processors.elasticsearch.PutElasticsearchRecord/index.html) afin de peupler votre index pdvevent.


### Configuration Kibana

Dans kibana, aller dans le menu "index management" afin de créer un nouveau pattern d'index 'pdvevent', vérifier :
 - que le champ datetime peut bien être utilisé comme champs date
 - que le champ location peut bien être utilisé comme géopoint 

Créer une dataview afin de pouvoir visualiser vos pdvevent en fonction du temps.

### Validation étape 6
- [ ] Ajout du mapping ElasticSearch OK
- [ ] Ecriture des données dans ElasticSearch via Nifi OK
- [ ] Configuration de l'index pdvevent dans Kibana OK
- [ ] Dataview Kibana OK


## Etape 7 : Création de dashboard Kibana

### Création d'une carte de France des pdv

Créer un graphique qui positionne l'ensemble des pdv sur une carte.


### Création des courbes d'évolution du prix

Créer un graphique qui montre l'évolution du prix de chaque carburant en fonction du temps.


### Créer un histogramme évolution des prix

Créer un graphique de type histogramme qui présente l'évolution du prix des carburants en France en moyenne par mois.

### Création d'un heatmap évolution des prix

Créer un graphique de type heatmap qui présente l'évolution du prix des carburants en France par département.

### Validation étape 7
- [ ] Visualisation map OK
- [ ] Visualisation courbe OK
- [ ] Visualisation histogramme  OK
- [ ] Visualisation heatmap OK


## Etape 8 : Gestion des ruptures de stock

Sans modifier ce qui a été fait ci dessus afin que cela reste fonctionnel.
Définir un nouvel evenement, ajouter des processors nifi, créer un nouvel index elastic avec son mapping.

### Objet 'rupturepdv'

Il contiendra les attributs suivants  :  
  - idpdv : identifiant numérique du pdv
  - location : location du pdv [GeoPoint](https://datatracker.ietf.org/doc/html/rfc7946#appendix-A.1)
  - datetime : date de la rupture [Datetime ISO 8601](https://en.wikipedia.org/wiki/ISO_8601#Times)
  - carburant : Enumération (Gazole, SP95, SP98, E10, E85, GPLc)
  - cp : code postal du pdv

### Création d'un top rupture

Créer une requête ESQL  qui affiche les 10 points de vente présentant le plus de rupture sur une année tout carburants confondus.

### Création d'un pie chart 

Créer un graphique de type pie chart qui présente le pourcentage de rupture pour chaque carburant vis à vis du nombre de ruptures totales.

### Validation étape 8
- [ ] Object rupturepdv OK
- [ ] Modification des processors nifi OK
- [ ] Insertion des rupture dans un nouvel index elasticsearch
- [ ] Requête ESQL top OK
- [ ] Visualisation pie chart OK

## Etape 9 : Notification en cas de rupture

Lorsqu'un carburant est en rupture envoyer une notification sur un canal slack si la rupture a eu lieu il y a moins de 5 jours.

### Créer un compte Slack

Créer un compte slack et une clé d'API qui pourra être utiliser par Nifi

### Ajouter la notification dans votre chaine de traitement

Utiliser le processor [PublishSlack](https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi/nifi-slack-nar/1.28.1/org.apache.nifi.processors.slack.PublishSlack/index.html) afin de pousser la notification sur un canal but3.

### Validation étape 9 
- [ ] Création Slack (clé API) OK
- [ ] Modification des processors nifi OK
- [ ] Apparition du message sur Slack OK





